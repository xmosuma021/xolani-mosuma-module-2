class AppPrint {
  AppPrint() {
    var appName = "Ambani Learn";
    var appCategory = "Education";
    var appDeveloper = "Mukundi Lambani";
    var appYear = "2021";

    print(
        "App Name: $appName, Sector/Category: $appCategory, App Developer: $appDeveloper, Year: $appYear");
    appNameToUpperCase(appName);
  }

  void appNameToUpperCase(String appName) {
    print(appName.toUpperCase());
  }
}

void main() {
  AppPrint();
}
